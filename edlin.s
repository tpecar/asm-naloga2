! 1 
! 1 # 1 "edlin.c"
! 1 # 166
! 166    
! 167 # 171
! 171    
! 172 # 179
! 179 unsigned char zaslon_stran; 
!BCC_EOS
! 180 unsigned char zaslon_nacin; 
!BCC_EOS
! 181 unsigned char zaslon_st_stolpcev; 
!BCC_EOS
! 182 
! 183 
! 184 
! 185 void pridobi_zaslon()
! 186 {
export	_pridobi_zaslon
_pridobi_zaslon:
! 187 #asm
!BCC_ASM
   mov   ax,   #0x0F00
   int   #0x10
   mov   [_zaslon_nacin],        al
   mov   [_zaslon_st_stolpcev],  ah
   mov   [_zaslon_stran],        bh
! 193 endasm
!BCC_ENDASM
! 194 }
ret
! 195 
! 196 
! 197 void izpisi_niz(niz)
! 198 char *niz;
export	_izpisi_niz
_izpisi_niz:
!BCC_EOS
! 199 {
! 200    ;
push	bp
mov	bp,sp
push	di
push	si
!BCC_EOS
! 201 #asm
!BCC_EOS
!BCC_ASM
_izpisi_niz.niz	set	8
.izpisi_niz.niz	set	4
   
   mov   bp,   [bp + .izpisi_niz.niz]
   xor   si,   si
   
   
   mov   ah,   #0x0E
   mov   bh,   [_zaslon_stran]
   xor   bl,   bl 
   mov   cx,   #0x1
   
   izpisi_niz.naslednja_crka:
   mov   al,   [bp + si]
   test  al,   al
   
   
   jz    izpisi_niz.konec_niza
   int   #0x10
   inc   si
   j     izpisi_niz.naslednja_crka
   izpisi_niz.konec_niza:
! 222 endasm
!BCC_ENDASM
!BCC_EOS
! 223 }
pop	si
pop	di
pop	bp
ret
! 224 # 231
! 231 unsigned char   st_argumentov;
!BCC_EOS
! 232 
! 233 unsigned char argumenti[4 ][20 ] = {0};
.data
export	_argumenti
_argumenti:
.byte	0
.blkb	$13
.blkb	$3C
!BCC_EOS
! 234 # 238
! 238 void argumenti_init()
! 239 {
.text
export	_argumenti_init
_argumenti_init:
! 240 #asm
!BCC_ASM
   
   mov   ah,   #0x51
   int   #0x21
   
! 245  250
   
   
   
   mov   es,   bx
   xor   cx,   cx
   eseg
   mov   cl,   [0x80] 
   xor   si,   si     
   
   xor   di,   di     
   xor   dl,   dl     
   
   
   argumenti_init.naslednji_znak:
      cmp   si,   cx
      je    argumenti_init.konec_interpretacije

      
      eseg
      mov   al,   [si + 0x82]
      cmp   al,   #0x20 
      
      jne   argumenti_init.znak_ni_delimiter

         
         inc   dl
         mov   ah,   #20 
         mov   al,   dl
         mul   ah
         mov   di,   ax
         
         inc   [_st_argumentov]
         j  argumenti_init.znak_delimiter
      argumenti_init.znak_ni_delimiter:
         
         mov   [di + _argumenti],   al
         inc   di
      argumenti_init.znak_delimiter:
      inc   si
      j     argumenti_init.naslednji_znak
   argumenti_init.konec_interpretacije:

   
   inc [_st_argumentov]
! 294 endasm
!BCC_ENDASM
! 295 }
ret
! 296 
! 297 
! 298 typedef unsigned char* DATOTEKA;
!BCC_EOS
! 299 # 311
! 311 DATOTEKA odpri(ime_datoteke, tip_dostopa)
! 312 unsigned char* ime_datoteke;
export	_odpri
_odpri:
!BCC_EOS
! 313 unsigned char tip_dostopa;
!BCC_EOS
! 314 {
! 315    ;
push	bp
mov	bp,sp
push	di
push	si
!BCC_EOS
! 316 #asm
!BCC_EOS
!BCC_ASM
_odpri.ime_datoteke	set	8
.odpri.ime_datoteke	set	4
_odpri.tip_dostopa	set	$A
.odpri.tip_dostopa	set	6
   mov   dx,   [bp + .odpri.ime_datoteke]
   mov   al,   [bp + .odpri.tip_dostopa]
   mov   ah,   #0x3D
   int   #0x21
   
! 322 endasm
!BCC_ENDASM
!BCC_EOS
! 323 }
pop	si
pop	di
pop	bp
ret
! 324 # 328
! 328 unsigned char predpomnilnik[512 ] = {0};
.data
export	_predpomnilnik
_predpomnilnik:
.byte	0
.blkb	$1FF
!BCC_EOS
! 329 # 333
! 333 unsigned int beri(dat, buffer, st_b)
! 334 DATOTEKA dat;
.text
export	_beri
_beri:
!BCC_EOS
! 335 unsigned char* buffer;
!BCC_EOS
! 336 unsigned int st_b;
!BCC_EOS
! 337 {
! 338    ;
push	bp
mov	bp,sp
push	di
push	si
!BCC_EOS
! 339 #asm
!BCC_EOS
!BCC_ASM
_beri.st_b	set	$C
.beri.st_b	set	8
_beri.dat	set	8
.beri.dat	set	4
_beri.buffer	set	$A
.beri.buffer	set	6
   mov   bx,   [bp + .beri.dat]
   mov   dx,   [bp + .beri.buffer]
   mov   cx,   [bp + .beri.st_b]
   
   mov   ah,   #0x3F
   int   #0x21
   
! 347 endasm
!BCC_ENDASM
!BCC_EOS
! 348 }
pop	si
pop	di
pop	bp
ret
! 349 
! 350 
! 351 int main()
! 352 {
export	_main
_main:
! 353    unsigned char t_argument;
!BCC_EOS
! 354    DATOTEKA dat; 
!BCC_EOS
! 355    unsigned int  prebranih_b; 
!BCC_EOS
! 356    unsigned char branje;
!BCC_EOS
! 357    
! 358    
! 359    argumenti_init();
push	bp
mov	bp,sp
push	di
push	si
add	sp,*-8
! Debug: func () void = argumenti_init+0 (used reg = )
call	_argumenti_init
!BCC_EOS
! 360    pridobi_zaslon();
! Debug: func () void = pridobi_zaslon+0 (used reg = )
call	_pridobi_zaslon
!BCC_EOS
! 361    
! 362    izpisi_niz("Argumenti programa:\r\n");
! Debug: list * char = .1+0 (used reg = )
mov	bx,#.1
push	bx
! Debug: func () void = izpisi_niz+0 (used reg = )
call	_izpisi_niz
inc	sp
inc	sp
!BCC_EOS
! 363    for(t_argument=0; t_argument<st_argumentov; t_argument++){
! Debug: eq int = const 0 to unsigned char t_argument = [S+$E-7] (used reg = )
xor	al,al
mov	-5[bp],al
!BCC_EOS
!BCC_EOS
jmp .4
.5:
! 364       izpisi_niz(argumenti[t_argument]);
! Debug: ptradd unsigned char t_argument = [S+$E-7] to [4] [$14] unsigned char = argumenti+0 (used reg = )
mov	al,-5[bp]
xor	ah,ah
mov	cx,*$14
imul	cx
mov	bx,ax
! Debug: cast * unsigned char = const 0 to [$14] unsigned char = bx+_argumenti+0 (used reg = )
! Debug: list * unsigned char = bx+_argumenti+0 (used reg = )
add	bx,#_argumenti
push	bx
! Debug: func () void = izpisi_niz+0 (used reg = )
call	_izpisi_niz
inc	sp
inc	sp
!BCC_EOS
! 365       izpisi_niz("\r\n");
! Debug: list * char = .6+0 (used reg = )
mov	bx,#.6
push	bx
! Debug: func () void = izpisi_niz+0 (used reg = )
call	_izpisi_niz
inc	sp
inc	sp
!BCC_EOS
! 366    }
! 367    if(st_argumentov>0)
.3:
! Debug: postinc unsigned char t_argument = [S+$E-7] (used reg = )
mov	al,-5[bp]
inc	ax
mov	-5[bp],al
.4:
! Debug: lt unsigned char = [st_argumentov+0] to unsigned char t_argument = [S+$E-7] (used reg = )
mov	al,-5[bp]
cmp	al,[_st_argumentov]
jb 	.5
.7:
.2:
! Debug: gt int = const 0 to unsigned char = [st_argumentov+0] (used reg = )
mov	al,[_st_argumentov]
test	al,al
je  	.8
.9:
! 368    {
! 369 #asm
!BCC_EOS
!BCC_ASM
_main.t_argument	set	7
.main.t_argument	set	-5
_main.prebranih_b	set	2
.main.prebranih_b	set	-$A
_main.dat	set	4
.main.dat	set	-8
_main.branje	set	1
.main.branje	set	-$B
      int #0x01
! 371 endasm
!BCC_ENDASM
!BCC_EOS
! 372       
! 373       dat = odpri(&argumenti[0],0);
! Debug: list int = const 0 (used reg = )
xor	ax,ax
push	ax
! Debug: list * [$14] unsigned char = argumenti+0 (used reg = )
mov	bx,#_argumenti
push	bx
! Debug: func () * unsigned char = odpri+0 (used reg = )
call	_odpri
add	sp,*4
! Debug: eq * unsigned char = ax+0 to * unsigned char dat = [S+$E-$A] (used reg = )
mov	-8[bp],ax
!BCC_EOS
! 374       
! 375       branje = 1;
! Debug: eq int = const 1 to unsigned char branje = [S+$E-$D] (used reg = )
mov	al,*1
mov	-$B[bp],al
!BCC_EOS
! 376       while(branje)
! 377       {
jmp .B
.C:
! 378          prebranih_b = beri(dat, predpomnilnik, 512 );
! Debug: list int = const $200 (used reg = )
mov	ax,#$200
push	ax
! Debug: list * unsigned char = predpomnilnik+0 (used reg = )
mov	bx,#_predpomnilnik
push	bx
! Debug: list * unsigned char dat = [S+$12-$A] (used reg = )
push	-8[bp]
! Debug: func () unsigned int = beri+0 (used reg = )
call	_beri
add	sp,*6
! Debug: eq unsigned int = ax+0 to unsigned int prebranih_b = [S+$E-$C] (used reg = )
mov	-$A[bp],ax
!BCC_EOS
! 379 
! 380          
! 381          if(prebranih_b < 512 )
! Debug: lt int = const $200 to unsigned int prebranih_b = [S+$E-$C] (used reg = )
mov	ax,-$A[bp]
cmp	ax,#$200
jae 	.D
.E:
! 382          {
! 383             branje = 0;
! Debug: eq int = const 0 to unsigned char branje = [S+$E-$D] (used reg = )
xor	al,al
mov	-$B[bp],al
!BCC_EOS
! 384             predpomnilnik[prebranih_b] = 0;
! Debug: ptradd unsigned int prebranih_b = [S+$E-$C] to [$200] unsigned char = predpomnilnik+0 (used reg = )
mov	bx,-$A[bp]
! Debug: eq int = const 0 to unsigned char = [bx+_predpomnilnik+0] (used reg = )
xor	al,al
mov	_predpomnilnik[bx],al
!BCC_EOS
! 385          }
! 386          
! 387          izpisi_niz(predpomnilnik);
.D:
! Debug: list * unsigned char = predpomnilnik+0 (used reg = )
mov	bx,#_predpomnilnik
push	bx
! Debug: func () void = izpisi_niz+0 (used reg = )
call	_izpisi_niz
inc	sp
inc	sp
!BCC_EOS
! 388       }
! 389    }
.B:
mov	al,-$B[bp]
test	al,al
jne	.C
.F:
.A:
! 390    else
! 391       izpisi_niz("Uporaba: EDLIN [IME_DATOTEKE]\r\n");
jmp .10
.8:
! Debug: list * char = .11+0 (used reg = )
mov	bx,#.11
push	bx
! Debug: func () void = izpisi_niz+0 (used reg = )
call	_izpisi_niz
inc	sp
inc	sp
!BCC_EOS
! 392    return 0;
.10:
xor	ax,ax
add	sp,*8
pop	si
pop	di
pop	bp
ret
!BCC_EOS
! 393 }
! 394 
! Register BX used in function main
.data
.11:
.12:
.ascii	"Uporaba: EDLIN [IME_DATOTEKE]"
.byte	$D,$A
.byte	0
.6:
.13:
.byte	$D,$A
.byte	0
.1:
.14:
.ascii	"Argumenti programa:"
.byte	$D,$A
.byte	0
.bss
.comm	_zaslon_stran,1
.comm	_zaslon_st_stolpcev,1
.comm	_st_argumentov,1
.comm	_zaslon_nacin,1

! 0 errors detected
