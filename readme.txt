=== Tadej Pečar, 63140188 ===
     2. projektna naloga

KAJ NAJ BI PROGRAM BIL
----------------------
Namen tega projekta je bil, da se izdela vrstično orientiran urejevalnik
besedilnih datotek, kakršen je bil recimo klasični EDLIN znotraj DOS,
vendar s to izboljšavo, da bi lahko urejal datoteke, ki so večje od
prostega konvecionalnega pomnilnika (seveda s to omejitvijo, da program
deluje v real mode načinu).

Več o sami implementaciji tega je zapisano v uvodnem komentarju kode, vendar če
na kratko opišem princip:
   - namesto da si datoteko v celoti naložimo v pomnilnik ter jo urejamo od tam,
     naložimo le trenutno urejano vrstico
   - v primeru, da se vrstica spremeni, se ta sprememba shrani v pomnilniku, ki
     ga dinamično alociramo
     (s tem je teoretična omejitev urejevalnika ta, da imamo lahko največ
      toliko neshranjenih sprememb, kolikor je prostega pomnilnika zanje)
   - s shranitvijo sprememb se vse spremembe zapišejo v datoteko ter se
     pomnilnik sprosti, s čimer lahko urejamo naprej brez zapiranja
     programa
   - ker si hranimo vsako spremembo posebej, jih lahko pred dejanskim zapisom
     v datoteko uporabniku tudi ponovno prikažemo oz. lahko uporabnik
     posamezno spremembo tudi razveljavi (undo, če se sprijaznimo s
     fragmentacijo ponnilnika, je lahko celo za poljubno spremembo, ne samo
     zadnjo)
   - z nekolio več volje ter dodatno sintakso bi lahko s tem urejal tudi več
     datotek hkrati ter med sabo (za vsako posebej bi sicer moral voditi svoj
     seznam sprememb)

KAJ PROGRAM DEJANSKO JE
-----------------------
Kot vedno, je tudi pri tem projektu bila organizacija mojega časa izjemna, in
tako imam žal bolj ali manj le idejo ter lupino implementacije.

Torej, kar program trenutno dejansko počne, je, da prejme datoteko kot
argument ter izpiše njeno vsebino.
Kratko in zabavno.

KAKO PROGRAM PREVESTI TER ZAGNATI
---------------------------------
Potrebujete bcc C prevajalnik ter pripadajoči zbirnik as86 - na Arch Linuxu se
tega pridobi z

   pacman -S dev86

na Debianu pa preko

   apt-get install bcc

nato lahko program prevedete z

   bcc -v -Md edlin.c -o edlin.com

ter ga poženete z
   
   dosbox -c "mount c $PWD" -c "c:" -c "EDLIN.COM TEST.TXT"

Nakar bi vam moral program izliti vse modrosti življenja, ki so zapisane
znotraj datoteke TEST.TXT
----

Celoten proces prevajanja ter izvajanja z debuggerjem lahko poženete preko
zagona priloženih skript

   . ./setpath.sh
   
nato pa za prevajanje poženete

   c

oziroma za izvajanje z debuggerjem

   d

KAJ SEDAJ IN KAM NAPREJ
-----------------------
Trenutna oblika programa je sicer nekoliko žalostna, vendar pa ga bom
predvidoma razvijal naprej.

Za bolj zvedave duše je repozitorij projekta na
   https://bitbucket.org/tpecar/asm-naloga2
   
Vsaj v času pisanja (17.6.2016) je gor bolj ali manj že tu priložena različica.
