   unsigned char t_argument;
   unsigned  t_znak;
   unsigned char nov_argument;

   for(t_argument=0, t_znak=0;
       t_argument<MAX_ARGUMENTOV && t_argument<st_argumentov && t_znak<127;
       t_znak++
      )
   {
      /* ce naletimo na null znak, gremo na naslednji argument */
      if(argumenti[0][t_znak]==0)
      {
         t_argument++;
         nov_argument = 1;
      }
      else if(nov_argument)
      {
         /* potem ko se preverijo pogoji, se lokacija prvega znaka novega
            argumenta shranijo v tabelo */
         nov_argument=0;
         argumenti[t_argument]=argumenti[0][t_znak];
      }
   }
