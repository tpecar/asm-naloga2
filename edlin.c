/* neka zelo osnovna razlicica bralnika ter po moznosti urejevalnika po vrsticah.
   Opis klasicne variante:
      https://support.microsoft.com/en-us/kb/67706
      http://www.8bs.com/submit/acornappspdf/216.pdf
   
   Osnovni plan, ki bi ga rad naredil je, da
   - izdelano stetje vrstic
   - izdelam sam vmesnik za urejanje po vrstici (BIOS scancode naprej, nazaj,
     spreminjanje niza)
   - izdelano branje datoteke
   - izdelano pisanje datoteke.
   
   Ker bi mi radi odpirali datoteke, ki so lahko vec kot 640 KB velike
   (ter vec, kot gre sploh fizicno v pomnilnik, ki ga DOS lahko naslavlja), bomo
   zavoljo hitrega dostopa do vrstic si samo shranili na katerih offsetih se
   nahajajo vrstice, nato pa bomo za zahtevano vrstico seekali nanjo ter
   prebrali vsebino do naslednje vrstice.
   
   Pri tem bomo za shranjevanje pozicij vrstic alocirali tabelo 8-bitnih
   offset razlik - razlike zato, ker drugace bi z dejanskim naslovom lahko
   hranili le vrstice za do 256 B datoteko, med tem ko (zaradi enostavnosti
   implementacije) predpostavimo, da je malo verjetno, da v DOS okolju naletimo
   na besedilno datoteko ki ima 256 znakov znotraj ene same vrstice.
   
   Z razlikami se lahko na podlagi znanega celotnega offseta trenutne vrstice
   premaknemo na naslednjo oz. prejsnjo prejsnjo.
      Resda to od nas zahteva, da moramo v primeru "random access" premika
      preracunati offset tako, da pristejemo/odstejemo razlike vseh vmesnih
      vrstic, vendar pa je to dokaj poceni operacija glede na prihranek v
      pomnilniku.
   
   Program gre pri zagonu skozi vec stopenj:
      - odpre datoteko ter preko seek operacije (na konec) ugotovi njeno dolzino
        (da vemo, koliksno obmocje pomnilnika alocirati)
      - alocira (preko DOS) bralni buffer (512 B recimo)
      - alociramo prostor za tabelo razlik offset-ov (pri cemer na zacetku
        alociramo za recimo 256 vrstic, kar pomeni 16 paragrafov)
      - seek-a na zacetek datoteke ter pricnemo z branjem blokov (po 512),
        pri tem steje stevilo vrstic (zaznava \r\n), hkrati ima stevca za
        razliko med offsetom prejsnje ter trenutne ter nove vrstice
         - ce je razlika med prejsnjo ter trenutno vecja od 256 (overflow),
           vrzemo napako (vrstice z vec kot 256 znaki namrec ne moremo opisati)
         - ce je razlika veljavna, zapisemo v tabelo ter povecamo indeks
            - ce smo zapolnili indeks, alociramo dodaten prostor
              (vsaj dokler je to zadnje alocirano polje, ter da nimamo se
               kaksnih drugih programov, recimo TSRe, ki bi med delovanjem
               kaj alocirali, bi morali biti sposobni alocirati dodaten prostor)
      - ko pridemo do EOF (brez tega, da bi naleteli na neveljavno vrstico ali
        pa bili neuspesni pri alokaciji),
        - alociramo edit buffer, v katerem si hranimo
            - stevilko vrstice
            - ali gre za novo vrstico ali spremembo obstojece
              (zato da vemo, ali vsebino dodamo pred stevilko obstojece vrstice
               ali vsebino obstojece vrstice dejansko nadomestimo s podano)
            - stevilo znakov
            - kazalec na tabelo znakov vrstice
            mi namrec ob spreminjanju ne zelimo takoj spreminjati prvotne
            datoteke - da pa preprecimo, da bi tale buffer postal prevelik,
            ga lahko eksplicitno storniramo s pisanjem v datoteko.
        - bralni buffer je po novem hkrati tudi urejevalni buffer
        - gremo v REPL fazo:
            - cakamo na ukaz, pri cemer so ti lahko:
               - [vrstica_pred] I -> Insert:
                     vstavi novo vrstico pred podano, ob zakljucku urejanja
                     trenutne vrstice gre samodejno na naslednjo (gremo ven
                     preko Ctrl+C)
                     pri tem imamo readline nacin urejanja (premikanje s
                     kurzorjem, v primeru predolge vrstice scrollanje
                     levo/desno)
                     
                     ko se vrstica vstavi, se vse ostale vrstice premaknejo za
                     mesto naprej - dejansko se mora vsakic ko se izpise
                     stevilka vrstice, iti skozi edit buffer, ter za vse
                     vrstice pred podano, ki se dodajajo, pristeti 1 k podani
                     vrstici
                     
               - [izvor_od],[izvor_do],[vrstica_pred] C -> Copy:
                     kopira podano obmocje vrstic pred podano vrstico
               - [od],[do] D -> Delete:
                     izbrise podano obmocje vrstic
                     (kar implementacijsko gledano to pomeni, je da se za
                      podane vrstice ustvarijo vnosi z dolzino znakov 0 ter
                      brez tabele)
                      
                      podobno kot pri dodajanju, ker se stevilo vrstic s tem
                      spremeni, moramo za vsako izbrisano vrstico pred podano
                      odsteti 1 od podane vrstice
               - [od],[do],[kam] M -> Move:
                     kombinirana operacija Copy & Delete
               - [vrstica]:
                     pricne z urejanjem podane vrstice, pri cemer imamo
                     readline nacin urejanja
                     
                     interpretacija stevilke vrstice gre glede na stanje
                     edit bufferja, nato pa gremo v njem od zadaj naprej
                     (zato da dobimo zadnjo spremembo) in v primeru, da:
                        - ze najdemo to vrstico v edit bufferju, skopiramo
                          njeno vsebino v urejevalni buffer ter urejamo to
                        - se ne najdemo vrstice v edit bufferju, preberemo
                          iz datoteke v bralni buffer ter urejamo tega
                              implementacijsko gledano bi to naredili tako, da
                              preberemo vsebino vrstice iz podanih offsetov,
                              nato pa bi spreminjali bralni buffer
                          
                          v obeh primerih shranimo spremenjeno vrstico v edit
                          buffer kot NOV VNOS
                          (cetudi je morda njena prejsnja verzija ze v edit
                           bufferju)
                     ce vsebine ne spremenimo (brez sprememb znakov), ne dodamo
                     v edit buffer
               - [od],[do] L -> List:
                     izpise podano obmocje vrstic, vkljucno s stevilkami vrstic
                     
                     pri tem moramo z upostevati spremembe iz edit bufferja
               - [koliko] C -> Changes:
                     izpise zadnjih nekaj podanih sprememb (po vrsticah) v
                     edit bufferju, ki se niso bile zapisane v datoteko, v obliki
                     [stevilka spremembe] [stevilka vrstice] : [tip spremembe] [vsebina]
                     pri cemer je tip spremembe oznacen z
                        + ce je bila vrstica dodana
                        - ce je bila vrstica izbrisana
                        ! ce je bila vrstica spremenjena
                     pri tem so spremembe predstavljene v kronoloskem zaporedju
                     (stevilka spremembe narasca)
                     
                     (brez podanega stevila izpise vse)
               - [koliko] U -> Undo:
                     stornira zadnjih nekaj podanih sprememb iz edit bufferja
                     (brez podanega stevila odstrani zadnjega)
                     
                     to lahko naredimo dokaj enostavno s tem da dealociramo
                     vsebino za posamezno vrstico v edit bufferju in ker se
                     omejimo le na zadnje spremembe, ne pridemo do fragmentacije
               - W -> Write:
                     zapise nazaj v datoteko (pri cemer ta NI enaka prvotni,
                     na zacetku ima neko temp ime), pri cemer mora sedaj za
                     vsako vrstico posebej iti skozi edit buffer, preveriti ce
                     se njena posodobljena stevilka vrstice
                        - ujema z vrstico v edit bufferju - v tem primeru
                          zapisemo vsebino vrstice iz edit bufferja
                        - ne ujemo z nobeno izmed vrstic edit bufferja - v tem
                          primeru vrstico prvotne datoteke zapisemo v izhodno
                     prvotno datoteko zapremo, izbrisemo, temp datoteko
                     preimenujemo vanjo ter storniramo edit buffer
               - Q -> Quit:
                     izhod iz programa, pri cemer zadnje spremembe, ki niso
                     bile eksplicitno shranjene preko W, zavrzemo
                     (storniramo edit buffer, dealociramo preostale strukture)
                     
      Tezava je le, da sedaj vsak premik med vrsticami tudi zahteva
      pregledovanje edit bufferja, da se bodisi pristeje ali odsteje za kako
      mesto, da preskocimo dodane ali brisane vrstice.
      Glede na to, da mora to poceti skoraj vsaka operacija, bi pri kake 100
      spremenjenih vrsticah lahko to postalo relativno pocasno
      (ceprav v tej tocki bi bilo tako ali drugace smiselno shraniti).
      
      
      
      To je definitivno za mnogo vec kot 3 dni dela.
      Ce sem cisto iskren, je to zanimiv poletni projekt, ne pa nekaj, kar
      bi se lahko dejansko naredilo pred izpitnim obdobjem.
*/

/* neuporabni komentarji z uporabno vsebino */
   /* sp ne moremo direktno uporabiti za absolutno/bazno naslavljanje, zato pa
      uporabimo bp, pri cemer preko njega tudi nalozimo naslov */
   /* preskocimo povratni naslov (zadnji na skladu), se premaknemo na zacetek
      argumenta
      pri tem seveda upostevamo dejstvo, da pri 8086 sklad narasca od visjih
      naslovov proti nizjim (torej za dostop do vrednosti na skladu pristejemo)
   */


/* ceprav ima bcc ze svojo (omejeno) knjiznico za vhod/izhod, se bomo omejili na
   servise, ki nam jih nudi DOS */

/* POMOZNE FUNKCIJE ZA V/I ZNOTRAJ DOS -------------------------------------- */

unsigned char zaslon_stran; /* active page */
unsigned char zaslon_nacin; /* display mode */
unsigned char zaslon_st_stolpcev; /* character column count */

/* pridobi podatke o zaslonu ter jih shrani v globalne spremenljivke
   (setup funkcija) */
void pridobi_zaslon()
{
#asm
   mov   ax,   #0x0F00
   int   #0x10
   mov   [_zaslon_nacin],        al
   mov   [_zaslon_st_stolpcev],  ah
   mov   [_zaslon_stran],        bh
#endasm
}

/* izpisi niz na podanem naslovu */
void izpisi_niz(niz)
char *niz;
{
   ;
#asm
   /* bp povozimo da kaze na tabelo (smo ga ze shranili na sklad) */
   mov   bp,   [bp + .izpisi_niz.niz]
   xor   si,   si
   
   /* parametri BIOS int 10h klica za izpis crke */
   mov   ah,   #0x0E
   mov   bh,   [_zaslon_stran]
   xor   bl,   bl /* brez atributov/ozadja */
   mov   cx,   #0x1
   
   izpisi_niz.naslednja_crka:
   mov   al,   [bp + si]
   test  al,   al
   
   /* izpisujemo dokler ne pridemo do null znaka */
   jz    izpisi_niz.konec_niza
   int   #0x10
   inc   si
   j     izpisi_niz.naslednja_crka
   izpisi_niz.konec_niza:
#endasm
}

/* POMOZNE FUNKCIJE ZA OBDELAVO ARGUMENTOV ---------------------------------- */

/* maksimalna velikost argumentov - za tem jih nehamo interpretirati */
#define MAX_ARGUMENTOV 4
#define MAX_DOLZINA_ARGUMENTA 20

unsigned char   st_argumentov;
/* kazalec na tabele argumentov (znotraj PSP)*/
unsigned char argumenti[MAX_ARGUMENTOV][MAX_DOLZINA_ARGUMENTA] = {0};

/* nastavi stevilo argumentov ter tvori tabelo - pri tem predpostavimo, da je
   DS se vedno nastavljen na zacetno vrednost (tekom programa se tipicno ne bi
   smel spreminjati, pri .COM datoteki ta namrec ostaja enak) */
void argumenti_init()
{
#asm
   /* klicemo DOS, da nam vrne lokacijo PSPja */
   mov   ah,   #0x51
   int   #0x21
   
   /* kar sem sicer tu zelel narediti, je, da bi naslov (segment pac) PSPja
    * prevedel na offset, ki bi bil zdruzljiv z DS, ki ga postavi prevajalnik
    * zal je prakticno nemogoce dolociti offset za naslov, ki je PRED trenutnim
    * DS (torej na negativni lokaciji), ker tudi ce pristejes negativno razliko
    * med segmentoma, bos dobil carry na na zgornjih 4ih bitih 20bitnega
    * naslova, posledicno pa drugacno lokacijo*/
   
   /* dobljen segment si zato shranimo v es, ter preko njega dostopamo do psp */
   mov   es,   bx
   xor   cx,   cx
   eseg
   mov   cl,   [0x80] /* st znakov niza */
   xor   si,   si     /* trenutni znak */
   
   xor   di,   di     /* trenutni indeks v tabeli argumentov */
   xor   dl,   dl     /* trenutni argument */
   
   /* interpretiramo argumente dokler ne zmanjka znakov */
   argumenti_init.naslednji_znak:
      cmp   si,   cx
      je    argumenti_init.konec_interpretacije
      /* niz se zacne sicer na 0x81, vendar je prvi znak vedno presledek, zato
         ga preskocimo */
      eseg
      mov   al,   [si + 0x82]
      cmp   al,   #0x20 /* presledek */
      /* ce se nimamo delimiterja, smo na istem argumentu */
      jne   argumenti_init.znak_ni_delimiter
         /* presledek - null nam ni potrebno dodajati ker je tabela na zacetku
            inicializirana na 0 */
         inc   dl
         mov   ah,   #MAX_DOLZINA_ARGUMENTA
         mov   al,   dl
         mul   ah
         mov   di,   ax
         /* povecamo stevilo argumentov */
         inc   [_st_argumentov]
         j  argumenti_init.znak_delimiter
      argumenti_init.znak_ni_delimiter:
         /* zapisemo znak v tabelo */
         mov   [di + _argumenti],   al
         inc   di
      argumenti_init.znak_delimiter:
      inc   si
      j     argumenti_init.naslednji_znak
   argumenti_init.konec_interpretacije:
   /* na koncu predvidoma ni delimiterja, zato povecamo stevilo argumentov, da
      zajamemo zadnjega */
   inc [_st_argumentov]
#endasm
}

/* kazalec na datoteko */
typedef unsigned char* DATOTEKA;

/* odpiranje datoteke - vrne kazalec na datoteko */
/* pri vseh naslednjih klicih predpostavimo, da je DS register ze nastavljen
   tako, da pravilno kaze na tabele! (torej se tekom izvajanja programa mora
   podatkovni segment ohranjati na taki lokaciji, kot jo pricakouje koda
   C prevajalnika) */
/*
   Mozni tipi dostopa so
   0 - branje
   1 - pisanje
   2 - branje in pisanje
*/
DATOTEKA odpri(ime_datoteke, tip_dostopa)
unsigned char* ime_datoteke;
unsigned char tip_dostopa;
{
   ;
#asm
   mov   dx,   [bp + .odpri.ime_datoteke]
   mov   al,   [bp + .odpri.tip_dostopa]
   mov   ah,   #0x3D
   int   #0x21
   /* vrnjen naslov je ze v ax - ta je hkrati return value */
#endasm
}

/* velikost bralnega predpomnilnika */
#define VELIKOST_PREDP 512

unsigned char predpomnilnik[VELIKOST_PREDP] = {0};

/* prebere podano stevilo bajtov iz datoteke, podane preko kazalca v podano
   tabelo */
/* vrne stevilo prebranih bajtov */
unsigned int beri(dat, buffer, st_b)
DATOTEKA dat;
unsigned char* buffer;
unsigned int st_b;
{
   ;
#asm
   mov   bx,   [bp + .beri.dat]
   mov   dx,   [bp + .beri.buffer]
   mov   cx,   [bp + .beri.st_b]
   
   mov   ah,   #0x3F
   int   #0x21
   /* ax ze vsebuje stevilo prebranih bajtov, return */
#endasm
}

/* VSTOPNA TOCKA PROGRAMA --------------------------------------------------- */
int main()
{
   unsigned char t_argument;
   DATOTEKA dat; /* kazalec na file handle */
   unsigned int  prebranih_b; /* stevilo prebranih bajtov */
   unsigned char branje;
   
   /* setup */
   argumenti_init();
   pridobi_zaslon();
   /* izvajanje */
   izpisi_niz("Argumenti programa:\r\n");
   for(t_argument=0; t_argument<st_argumentov; t_argument++){
      izpisi_niz(argumenti[t_argument]);
      izpisi_niz("\r\n");
   }
   if(st_argumentov>0)
   {
#asm
      int #0x01
#endasm
      /* poskusimo odpreti podano ime datoteke (1. argument) za branje */
      dat = odpri(&argumenti[0],0);
      /* poskusimo z branjem */
      branje = 1;
      while(branje)
      {
         prebranih_b = beri(dat, predpomnilnik, VELIKOST_PREDP);
         /* ce prisli do EOF, dodamo NULL znak na konec da ne izpisemo
            bogus podatkov od prej */
         if(prebranih_b < VELIKOST_PREDP)
         {
            branje = 0;
            predpomnilnik[prebranih_b] = 0;
         }
         /* izpisemo prebrano */
         izpisi_niz(predpomnilnik);
      }
   }
   else
      izpisi_niz("Uporaba: EDLIN [IME_DATOTEKE]\r\n");
   return 0;
}
